package Client.de.david;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

import Client.de.david.interfaces.IRenderable;
import Client.de.david.interfaces.IUpdatable;
import Client.de.david.screens.GameScreen;

public class Game extends JPanel implements Runnable, IRenderable, IUpdatable {

	private static final long serialVersionUID = -5412432850901731992L;

	private static final double FRAME_CAPTION = 1e9 / /* Frames pro Sekunde */ 60;
	public Random rnd = new Random();
	private ArrayList<IRenderable> renderListener = new ArrayList<IRenderable>();
	private ArrayList<IUpdatable> updateListener = new ArrayList<IUpdatable>();
	public static ZonedDateTime zdt = ZonedDateTime.now();
	private static Game instance;

	private boolean running = true;

	private BufferedImage imageBuffer;
	private Graphics graphics;

	private long fps, tps;
	
	public GameScreen gameScreen;

	public Game() {
		Game.instance = this;
		new Thread(this).start();
	}

	private void init() throws UnknownHostException, IOException {
		//Offscreen Puffer vorbereiten
		this.imageBuffer = new BufferedImage(Start_Client.WIDTH, Start_Client.HEIGHT, BufferedImage.TYPE_INT_RGB);
		this.graphics = imageBuffer.getGraphics();
		this.setSize(new Dimension(Start_Client.WIDTH, Start_Client.HEIGHT));
		this.initScenes();
	}

	private void initScenes() throws UnknownHostException, IOException {
		this.renderListener.add(gameScreen = new GameScreen());
	}
	
	public void run() {
		try {
			this.init();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		int ticks = 0;
		int frames = 0;

		long lastTime = System.nanoTime();
		long time = System.currentTimeMillis();

		double frameTime = 0.0D;

		boolean canRender = false;

		while (this.running) {
			long now = System.nanoTime();

			frameTime += (now - lastTime) / FRAME_CAPTION;
			lastTime = now;

			if (frameTime >= 1) {
				this.onUpdate();

				frameTime--;
				ticks++;

				canRender = true;
			} else
				canRender = false;

			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (canRender) {
				this.onRender(graphics);
				
				frames++;
			}

			if (System.currentTimeMillis() - 1000 >= time) {
				time += 1000;
				
				this.fps = frames;
				this.tps = ticks;

				frames = 0;
				ticks = 0;
			}
		}
	}

	@Override
	public void onUpdate() {
		this.updateListener.forEach(listener -> listener.onUpdate()); // Objekte
																		// updaten
	}

	@Override
	public void onRender(Graphics graphics) {
		if (this.getGraphics() != null) {
			this.renderListener.forEach(listener -> listener.onRender(graphics)); // Objekte
																					// auf
																					// den
																					// Puffer
																					// Zeichnen

			this.getGraphics().drawImage(imageBuffer, 0, 0, null); // Puffer auf
																	// den
																	// Bildschirm
																	// zeichnen
		}
	}

	public long getFPS() {
		return fps;
	}
	
	public long getTPS() {
		return tps;
	}
	
	public GameScreen getGameScreen() {
		return gameScreen;
	}
	
	public static Game getGame() {
		return instance;
	}
}
