package Client.de.david;

import java.io.IOException;

import javax.swing.JFrame;

import Client.de.david.input.Keyboard;

public class Start_Client {

	public final static String PREFIX = "[Client] ";
	public final static int WIDTH = 800;
	public final static int HEIGHT = 600;
	public final static String TITEL = "Spiel mit Sockets";
	
	public static String Server_IP = "127.0.0.1";
	public static int Server_Port = 1234;
	
	public static void main(String[] args) throws IOException {
		Game game = new Game();
		JFrame frame = new JFrame(Start_Client.TITEL);
		frame.addKeyListener(new Keyboard());
		frame.setSize(Start_Client.WIDTH, Start_Client.HEIGHT);
		frame.add(game);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
