package Client.de.david.entitys;

import java.awt.Color;
import java.awt.Graphics;

import Client.de.david.interfaces.IRenderable;
import Client.de.david.interfaces.IUpdatable;

public abstract class Mobile implements IRenderable, IUpdatable {
	
	private String name;
	
	private int posX, posY;
	private int width, height;
	
	private Color color;
	
	public Mobile(String name, int PosX, int PosY, int width, int height, Color color) {
		this.name = name;
		this.posX = PosX;
		this.posY = PosY;
		this.width = width;
		this.height = height;
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void onRender(Graphics g) {
		g.setColor(this.color);
		g.fillRect(this.posX, this.posY, this.width, this.height);
	}
}
