package Client.de.david.entitys;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

import Client.de.david.Game;
import Client.de.david.input.Keyboard;

public class Player extends Mobile {

	private int playerId;
	
	public Player() {
		super("Player", 100, 100, 20, 20, Color.RED);
	}

	@Override
	public void onRender(Graphics g) {
		super.onRender(g);
		
		g.drawString(this.getName(), this.getPosX() - 5, this.getPosY() - 5);
		
		if (Keyboard.isKeyPressed(KeyEvent.VK_W) || Keyboard.isKeyPressed(KeyEvent.VK_UP)) {
			this.setPosY(this.getPosY() - 2);
		}
		if (Keyboard.isKeyPressed(KeyEvent.VK_A) || Keyboard.isKeyPressed(KeyEvent.VK_LEFT)) {
			this.setPosX(this.getPosX() - 2);
		}
		if (Keyboard.isKeyPressed(KeyEvent.VK_S) || Keyboard.isKeyPressed(KeyEvent.VK_DOWN)) {
			this.setPosY(this.getPosY() + 2);
		}
		if (Keyboard.isKeyPressed(KeyEvent.VK_D) || Keyboard.isKeyPressed(KeyEvent.VK_RIGHT)) {
			this.setPosX(this.getPosX() + 2);
		}
		try {
			Game.getGame().gameScreen.client.sendPosition(this.getPosX(), this.getPosY());
		} catch (Exception e) {
		}

	}

	@Override
	public void onUpdate() {
		try {
		} catch (Exception e) {
		}
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
}
