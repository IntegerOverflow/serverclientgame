package Client.de.david.entitys;

import java.awt.Color;
import java.awt.Graphics;

import Client.de.david.interfaces.IRenderable;

public class OnlinePlayer extends Mobile implements IRenderable {

	private int playerId;
	
	public OnlinePlayer(String name, int playerId, int x, int y) {
		super(name, x, y, 20, 20, Color.BLUE);
		this.playerId = playerId;
		this.setName(name);
	}

	@Override
	public void onRender(Graphics g) {
		super.onRender(g);
		g.drawString(this.getName(), this.getPosX() + 5, this.getPosY() - 10);
		
	}

	@Override
	public void onUpdate() {}

	public int getPlayerId() {
		return playerId;
	}
	
}
