package Client.de.david.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import Client.de.david.Game;
import Client.de.david.entitys.OnlinePlayer;

public class PacketPosition extends Packet {

	public int X;
	public int Y;
	public int ID;
	
	public PacketPosition() {
		super((byte) 1);
	}

	public PacketPosition(int ID, int X, int Y) {
		super((byte)1);
		this.ID = ID;
		this.X = X;
		this.Y = Y;
	}
	
	
	@Override
	public void Send(DataOutputStream out) throws IOException {
		super.Send(out);
		out.writeInt(Game.getGame().gameScreen.getPlayer().getPlayerId());
		out.writeInt(Game.getGame().gameScreen.getPlayer().getPosX());
		out.writeInt(Game.getGame().gameScreen.getPlayer().getPosY());
		out.flush();
	}
	
	@Override
	public void Receive(DataInputStream in) throws IOException {
		int ID = in.readInt();
		int X = in.readInt();
		int Y = in.readInt();
		
		System.out.println(ID + " " + X + " " + Y);
		if (ID > -1 && ID < 1000) {
			System.out.println(Game.getGame().gameScreen.getOnlinePlayers().size());
			if (!(X < 0) && !(X > 800) && !(Y < 0) && !(Y > 600)) {
				boolean aviable = false;
				for (OnlinePlayer player : Game.getGame().gameScreen.getOnlinePlayers()) {
					if (player.getPlayerId() == ID || !(ID < 0)) {
						aviable = true;
						break;
					}
				}
				
				if (aviable == false && ID > 0 && ID != Game.getGame().gameScreen.getPlayer().getPlayerId()) {
					Game.getGame().gameScreen.getOnlinePlayers().add(new OnlinePlayer("Online" + Game.getGame().rnd.nextInt(Integer.MAX_VALUE), ID, X, Y));
				}
				
				for (OnlinePlayer player : Game.getGame().gameScreen.getOnlinePlayers()) {
					if (player.getPlayerId() == ID) {
						player.setPosX(X);
						player.setPosY(Y);
						break;
					}
				}
			}
			
		}
		
		
	}
	
}
