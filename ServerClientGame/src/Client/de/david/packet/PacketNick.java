package Client.de.david.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import Client.de.david.Game;

public class PacketNick extends Packet {
	
	public String Name;
	public int ID;
	
	public PacketNick(String Name, int ID) {
		super((byte)3);
		this.Name = Name;
		this.ID = ID;
	}
	
	public PacketNick(int ID) {
		super((byte)3);
		this.Name = "Default";
		this.ID = ID;
	}
	
	public PacketNick() {
		super((byte)3);
		this.Name = "Default";
		this.ID = 999;
	}


	@Override
	public void Send(DataOutputStream out) throws IOException {
		super.Send(out);
//		out.writeInt(this.ID);
//		out.flush();
	}
	
	@Override
	public void Receive(DataInputStream in) throws IOException {
		int PlayerID = in.readInt();
		Game.getGame().gameScreen.getPlayer().setPlayerId(PlayerID);
	}

}
