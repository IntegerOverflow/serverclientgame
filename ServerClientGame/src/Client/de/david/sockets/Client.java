package Client.de.david.sockets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

import Client.de.david.Game;
import Client.de.david.packet.Packet;
import Client.de.david.packet.PacketNick;
import Client.de.david.packet.PacketPosition;

public class Client implements Runnable {

	private Socket socket;
	private String name;

	public DataInputStream in;
	public DataOutputStream out;
	
	public int ID = Integer.MAX_VALUE;

	public Client(Socket socket, String name) throws Exception {
		this.name = name;
		this.socket = socket;
		this.in = new DataInputStream(this.socket.getInputStream());
		this.out = new DataOutputStream(this.socket.getOutputStream());
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public String getName() {
		return name;
	}

	
	public synchronized void sendPosition(int X, int Y) throws Exception {
		PacketPosition posPacket = new PacketPosition(Game.getGame().gameScreen.getPlayer().getPlayerId(),
				Game.getGame().gameScreen.getPlayer().getPosX(), Game.getGame().gameScreen.getPlayer().getPosY());
		posPacket.Send(out);
	}
	
	@Override
	public void run() {
		new Thread(){
			@Override
			public void run() {
				while (true) {
					try {
						Game.getGame().gameScreen.client.sendPosition(Game.getGame().gameScreen.getPlayer().getPosX(), Game.getGame().gameScreen.getPlayer().getPosY());
						Thread.sleep(250);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				
			}
		}.start();
		try {
			while (true) {
					final byte packetId = in.readByte();
					Packet currentPacket = null;
					
					for (Packet p : Game.getGame().gameScreen.Packets) {
						if (p.getPacketID() == packetId) {
							currentPacket = p;
							break;
						}
					}
					
					if (currentPacket != null) {
						if (currentPacket instanceof PacketNick) {
							((PacketNick)currentPacket).Receive(in);
						}
						else if (currentPacket instanceof PacketPosition) {
							((PacketPosition)currentPacket).Receive(in);
						}
					}
					
//					switch (packetId) {
//					case 0:
//						// Login
//						break;
//					case 1:
//						
//						
//						
//						break;
//					case 2:
//						
//						break;
//					case 3:
//						break;
//					}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			System.out.println("Server offline! I hope you had fun :-)");
		}
		
	}

}
