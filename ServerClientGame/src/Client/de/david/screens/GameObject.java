package Client.de.david.screens;

import java.awt.Color;
import java.awt.Graphics;

import Client.de.david.Start_Client;
import Client.de.david.interfaces.IRenderable;
import Client.de.david.interfaces.IUpdatable;

public abstract class GameObject implements IRenderable, IUpdatable {
	
	private String name;
	private boolean fullscreen = true;
	
	private int width = Start_Client.WIDTH;
	private int height = Start_Client.HEIGHT;
	
	private Color BackgroundColor;
	
	
	@Override
	public void onRender(Graphics g) {
		if (this.fullscreen) {
			g.setColor(this.BackgroundColor);
			g.fillRect(0, 0, this.width, this.height);			
		}
	}
	
	public Color getBackgroundColor() {
		return BackgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		BackgroundColor = backgroundColor;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public GameObject(String name, boolean fullscreen, Color color) {
		this.name = name;
		this.fullscreen = fullscreen;
		this.BackgroundColor = color;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setFullscreen(boolean value) {
		this.fullscreen = value;
	}
	
	public boolean isFullscreen() {
		return this.fullscreen;
	}
}
