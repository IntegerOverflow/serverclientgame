package Client.de.david.screens;

import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import Client.de.david.Start_Client;
import Client.de.david.entitys.OnlinePlayer;
import Client.de.david.entitys.Player;
import Client.de.david.packet.Packet;
import Client.de.david.packet.PacketNick;
import Client.de.david.packet.PacketPosition;
import Client.de.david.sockets.Client;

public class GameScreen extends GameObject {

	private Player mainPlayer;
	private ArrayList<OnlinePlayer> onlinePlayers = new ArrayList<OnlinePlayer>();
	public Client client;
	public ArrayList<Packet> Packets = new ArrayList<Packet>();
	
	public void registerPackets() {
		this.Packets.add(new PacketNick());
		this.Packets.add(new PacketPosition());
	}
	
	
	public GameScreen() throws UnknownHostException, IOException {
		super("GameScene",true,Color.BLACK);
		this.mainPlayer = new Player();
		this.registerPackets();
		try {
			this.client = new Client(new Socket(Start_Client.Server_IP, Start_Client.Server_Port), this.mainPlayer.getName());	
			new Thread(client).start();
		} catch (Exception e) {
		}
//		new Thread(new GameScreen.PositionSender()).run();
	}

	@Override
	public void onRender(Graphics g) {
		super.onRender(g);
		g.setColor(Color.YELLOW);
		if (this.client != null) {
			g.drawString("Connected with " + Start_Client.Server_IP + ":" + String.valueOf(Start_Client.Server_Port), 5, 20);
		}
		else
		{
			g.drawString("Connection error!",  5, 20);
		}
		try {
			this.onlinePlayers.forEach(player -> player.onRender(g));			
		} catch (Exception e) {
		}
		this.mainPlayer.onRender(g);
		this.onUpdate();
	}


	public synchronized ArrayList<OnlinePlayer> getOnlinePlayers() {
		return onlinePlayers;
	}
	
	public Player getPlayer() {
		return mainPlayer;
	}


	@Override
	public void onUpdate() {
		
	}

	
	
	
}
