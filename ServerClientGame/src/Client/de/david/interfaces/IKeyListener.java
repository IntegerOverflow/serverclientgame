package Client.de.david.interfaces;

import java.awt.event.KeyEvent;

public interface IKeyListener {

	public void onKeyPressed(KeyEvent e);
	public void onKeyReleased(KeyEvent e);
	public void onKeyTyped(KeyEvent e);
	
}
