package Client.de.david.interfaces;

import java.awt.Graphics;

public interface IRenderable {
	public abstract void onRender(Graphics g);
}
