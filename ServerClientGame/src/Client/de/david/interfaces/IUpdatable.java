package Client.de.david.interfaces;

public interface IUpdatable {
	public abstract void onUpdate();
}
