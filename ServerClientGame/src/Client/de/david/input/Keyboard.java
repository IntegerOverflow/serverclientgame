package Client.de.david.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import Client.de.david.interfaces.IKeyListener;

public final class Keyboard implements KeyListener {

	private static final boolean[] keys;
	private static final ArrayList<IKeyListener> listenerlist;

	public static void KeyReleased(int key) { 
		keys[key] = false; 
	}
	
	public static void KeyPressed(int key) {
		keys[key] = true; 
	}

	public static boolean isKeyPressed(int key) { 
		return keys[key]; 
	}

	static {
		keys = new boolean[32767];
		listenerlist = new ArrayList<IKeyListener>();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		Keyboard.KeyPressed(e.getKeyCode());
		for (IKeyListener l : listenerlist) {
			l.onKeyPressed(e);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Keyboard.KeyReleased(e.getKeyCode());
		for (IKeyListener l : listenerlist) {
			l.onKeyReleased(e);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		Keyboard.KeyPressed(e.getKeyCode());
		for (IKeyListener l : listenerlist) {
			l.onKeyTyped(e);
		}
	}

}
 