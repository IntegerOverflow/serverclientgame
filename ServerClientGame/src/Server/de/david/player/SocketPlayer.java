package Server.de.david.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import Server.de.david.Server;
import Server.de.david.packet.Packet;
import Server.de.david.packet.PacketNick;
import Server.de.david.packet.PacketPosition;

public class SocketPlayer extends Thread {

	private final Socket socket;
	private String name = "Unnamed";

	private final int playerId;
	
	public int posX = 0;
	public int posY = 0;

	public DataInputStream in;
	public DataOutputStream out;
	
	public SocketPlayer(Socket socket, String name, int playerId, int x, int y) {
		this.socket = socket;
		this.name = name;
		
		this.playerId = playerId;
		
		this.posX = x;
		this.posY = y;

		try {
			this.in = new DataInputStream(this.socket.getInputStream());
			this.out = new DataOutputStream(this.socket.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.start();
	}

	@Override
	public void run() {
		PacketNick nickPacket = new PacketNick();
		try {
			nickPacket.ID = this.playerId;
			nickPacket.Send(out);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		while (true) {
			try {
				final byte packetId = this.in.readByte();
				Packet currentPacket = null;
				for (Packet p : Server.getServer().Packets) {
					if (p.getPacketID() == packetId) {
						currentPacket = p;
						break;
					}
				}
				
				if (currentPacket != null) {
					if (currentPacket instanceof PacketNick) {
						((PacketNick)currentPacket).ID = this.playerId;
						((PacketNick)currentPacket).Receive(in);
						
					}
					else if (currentPacket instanceof PacketPosition) {
						((PacketPosition)currentPacket).Receive(in);
					}
					else
					{
						System.out.println("Unknown Data :(");
					}
				}
				
				
			} catch (IOException e) {
				break;
			}
		}
		Server.getServer().getClients().remove(this);
		System.out.println("[" + java.util.Date.from( Server.zdt.toInstant()) + "] " + "Disconnect: " + this.getSocket().getInetAddress().getHostAddress().replace("/", "") + ":" + this.getSocket().getRemoteSocketAddress().toString() + " | Bye... :(");
	}

	public DataInputStream getIn() {
		return in;
	}

	public DataOutputStream getOut() {
		return out;
	}

	public Socket getSocket() {
		return socket;
	}

	public String getPName() {
		return name;
	}

	public void setPName(String name) {
		this.name = name;
	}

	public void setLocation(int x, int y) {
		this.posX = x;
		this.posY = y;
	}
	
	public int getX() {
		return posX;
	}

	public void setX(int x) {
		posX = x;
	}

	public int getY() {
		return posY;
	}

	public void setY(int y) {
		posY = y;
	}

	public int getPlayerId() {
		return playerId;
	}
}
