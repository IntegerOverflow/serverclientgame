package Server.de.david;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;

import Server.de.david.packet.Packet;
import Server.de.david.packet.PacketNick;
import Server.de.david.packet.PacketPosition;
import Server.de.david.player.SocketPlayer;

public class Server extends Thread{

	private static int PORT = 1234;

	private static Server instance;
	public static ZonedDateTime zdt = ZonedDateTime.now();
	private ServerSocket socket;
	private ArrayList<SocketPlayer> connectedClients;
	public ArrayList<Packet> Packets = new ArrayList<Packet>();
	
	public int ClientID = 5;
	
	private void RegisterPackets() {
		this.Packets.add(new PacketPosition());
		this.Packets.add(new PacketNick());
	}
	
	public Server() {
		super("Server Thread");
		connectedClients = new ArrayList<SocketPlayer>();
		instance = this;
	}
	
	
	
	@Override
	public void run() {
		try {
			this.socket = new ServerSocket(PORT);
			this.RegisterPackets();
			System.out.println("Server started! Port: " + String.valueOf(PORT));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (PORT == 0) {
			System.out.println("Invalid Port! Please change your config.txt"); // TODO: Kleines Config-System wird bald vorhanden sein
			return;
		}
		while (true) {
			try {
				if (this.getClients().size() < 2) { // Maximal 2 Verbindungen
					final Socket soc = socket.accept(); // Warten auf n�chste Verbindung
					SocketPlayer newPlayer = new SocketPlayer(soc, "DefaultPlayer", this.ClientID++, 100, 100);
					this.getClients().add(newPlayer);
					System.out.println("[" + Date.from(zdt.toInstant()) + "] " + "Connect: "
							+ soc.getInetAddress().getHostAddress().replace("/", "") + ":"
							+ soc.getRemoteSocketAddress().toString() + " | Welcome! :)");

				}
			} catch (Exception e) {
			}

		}
	}
		
	public static Server getServer() {
		return instance;
	}
	
	public synchronized boolean setNameOfClient(int ID, String Name) {
		System.out.println("Search for ID: " + ID);
		for (SocketPlayer user : this.getClients()) {
			System.out.println("Found: " + user.getPlayerId());
			if (user.getPlayerId() == ID) {
				System.out.println(Name);
				user.setName(Name);
				return true;
			}
		}
		return false;
	}
	
	public synchronized boolean setPositionOfClient(int ID, int X, int Y) {
		for (SocketPlayer user : this.getClients()) {
			if (user.getPlayerId() == ID ) {
				user.posX = X;
				user.posY = Y;
				return true;
			}
		}
		return false;
	}
	
	public synchronized SocketPlayer getClient(int ID) {
		for (SocketPlayer user : this.getClients()) {
			if (user.getPlayerId() == ID) {
				return user;
			}
		}
		return null;
	}
	
	public synchronized ArrayList<SocketPlayer> getClients() {
		return this.connectedClients;
	}
}
