package Server.de.david.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketNick extends Packet {
	
	public int ID;

	public PacketNick() {
		super((byte)3);
	}
	

	
	@Override
	public void Send(DataOutputStream out) throws IOException {
		super.Send(out);
		out.writeInt(this.ID);
		out.flush();
	}
	
	@Override
	public void Receive(DataInputStream in) throws IOException {}

}
