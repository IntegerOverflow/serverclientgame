package Server.de.david.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import Server.de.david.Server;
import Server.de.david.player.SocketPlayer;

public class PacketPosition extends Packet {

	public int X;
	public int Y;
	public int ID;
	
	public PacketPosition() {
		super((byte) 1);
	}

	public PacketPosition(int ID, int X, int Y) {
		super((byte)1);
		this.ID = ID;
		this.X = X;
		this.Y = Y;
	}
	
	
	@Override
	public void Send(DataOutputStream out) throws IOException {
		super.Send(out);
	}
	
	@Override
	public void Receive(DataInputStream in) throws IOException {
		int ID = in.readInt();
		int X = in.readInt();
		int Y = in.readInt();
//		System.out.println("1" + "ID: " + ID + " X: " + X + " Y: " + Y);
		Server.getServer().setPositionOfClient(ID, X, Y);
		
		for (SocketPlayer player : Server.getServer().getClients()) {
			player.out.writeByte((byte) 1);
			player.out.writeInt(ID);
			player.out.writeInt(X);
			player.out.writeInt(Y);
			player.out.flush();
		}
		
		
	}
	
}
