package Server.de.david.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class Packet {

	private byte PacketID;

	public byte getPacketID() {
		return this.PacketID;
	}
	
	public Packet(byte PacketID) {
		this.PacketID = PacketID;
	}
	
	public void Send(final DataOutputStream out) throws IOException {
		out.writeByte(this.PacketID);
	}
	
	public void Receive(final DataInputStream in) throws IOException {}
	
}
